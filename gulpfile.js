'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const debug = require('gulp-debug');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer'); // Добавляет прификсы браузеров
const remember = require('gulp-remember'); // Запоминание файлов в кеше при обновлении
const plumber = require('gulp-plumber'); // Обработчик ошибок
const path = require('path');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');


const browserSync = require('browser-sync').create();

const del = require('del');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

gulp.task('styles',function (callback) {
    gulp.src(['dev/**/reset.css'],{since:gulp.lastRun('styles')})
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('prod'));


    gulp.src(['dev/**/*.sass','dev/**/*.scss'],{since:gulp.lastRun('styles')})
            .pipe(plumber())
            .pipe(remember('styles'))
            .pipe(sass())
            .pipe(autoprefixer())
            .pipe(concat('css/style.css'))
                .pipe(cssmin())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('prod'));
      callback();

});

gulp.task('styles_prod',function (callback) {
    gulp.src(['dev/**/*.sass','dev/**/*.scss'],{since:gulp.lastRun('styles')})
        .pipe(remember('styles'))
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(concat('css/style.css'))
        .pipe(gulp.dest('dev'));

    gulp.src('dev/**/*.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('prod'));

    callback();
});

gulp.task('assets',function () {
    return gulp.src('dev/assets/**',{since: gulp.lastRun('assets')})
        .pipe(debug({title:'assets'}))
        .pipe(gulp.dest('prod'));
});

gulp.task('index',function () {
    return gulp.src('dev/**/*.html',{since : gulp.lastRun('index')})
        .pipe(gulp.dest('prod'));
});

gulp.task('clean', function () {
    return del('prod');
});

gulp.task('watch',function () {
    gulp.watch(['dev/**/*.sass','dev/**/*.scss'],gulp.series('styles')).on('unlink',function (filepath) {
        remember.forget('styles',path.resolve(filepath))
    });
    gulp.watch('dev/assets/**/*.*',gulp.series('assets'));

    gulp.watch('dev/**/*.html',gulp.series('index'));
});

gulp.task('build',gulp.series(
    'clean', gulp.parallel('styles','assets','index')
));



gulp.task('server',function () {
    browserSync.init({
        server:'prod'
       // port: 8080
    });
    browserSync.watch('prod/**/*.*').on('change',browserSync.reload)
});

gulp.task('dev',gulp.series('build',gulp.parallel('watch','server')));

gulp.task('default', gulp.series('styles_prod','assets','index'));